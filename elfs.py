class Elf:
    def __init__(self, id, name, level):
        self.identyfikator = id
        self.name = name
        self.poziom = level

    def __str__(self):
        return self.identyfikator + " " + self.name + ";" + str(self.poziom)
